# Routing Atomic Service

OpenTripPlanner as an Atomic Service in Synchronicity

# Introduction
The Routing atomic service allows citizens to execute queries for finding routes, bus and taxi stops, city bikes and bicycling routes, disruption info. In addition it is useful for generating multi-modal routing taking into consideration static and dynamic information coming from the cities such as air quality or traffic estimation. 
This atomic service is based on the OpenTripPlanner (OTP, http://www.opentripplanner.org/), a family of Open Source Software projects developed by the OpenTripPlanner community. Requests to this service can be done either through the original OTP API or via GraphQL. The latter is a specific customization of the general service.
City information can be injected via the SynchroniCity framework with the usage of other SynchroniCity atomic services such as GTFS Fetcher, GTFS-RT Loader from NGSI, and Urban Mobility to GTFS.
# Architecture
The OpenTripPlanner creates routes based on map and transport schedule and route data. The main data formats are General Transit Feed Specification (GTFS) and OpenStreetMap data (OSM), which OTP supports natively. It also accepts real time updates via GTFS Real Time (GTFS-RT).
![Routing Atomic Service Architecture](images/OTP_Synchronicity_Architecture_Highlevel.png)
# Installation
1.	Install Docker

```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg|sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
```

2.	Get GTFS data

The best option is to get the data directly from transit operator or agency but Transitland  and TransiFeeds  services also provide lists of feeds.
If you are going to use data from Helsinki (HSL) use following command:

```
wget http://dev.hsl.fi/gtfs/hsl.zip
```

If your local public transport schedule data is not available in GTFS format, you need to do this formatting yourself. Please follow the guidelines by gtfs.org .

3.	Get OpenStreetMap data

You also need OSM data for the same area as the GTFS -data is from. You can download it from the Geofrabrik’s servers .
Files provided by the Geofabrik are quite large, so you need to trim them futher with osmconvert.
To do this, you need to figure out the bounding box you are interested in. This can be done for example with the bounding box tool . The CSV (Comma Separated Values) option in that tool produces exactly the format expected by the osmconvert -b switch.
For example, this is how you would get data for Helsinki Region:
Download OSM-data for Finland:

```
wget http://download.geofabrik.de/europe/finland-latest.osm.pbf
Clip Helsinki region with osmconvert:
osmconvert finland-latest.osm.pbf -b=24.002,59.9774,25.7323,60.6951 --complete-ways -o=helsinki.pbf
```

4.	Start OTP using a Docker image

Before you start the OTP, create new folder for the data. E.g. ~/otpdata and copy pbf and gtfs files into that. e.g.

```
mkdir ~/otpdata
cp helsinki.pbf hsl.zip ~/otpdata
```

Then you simply run the OTP from the Docker image, which is automatically fetched from the Maven repository and installed to your local filesystem:

```
docker run -e JAVA_MX=8G -v ~/otpdata:/data:ro -p 8080:8080 opentransitmap/opentripplanner run.sh --build /data --inMemory
```
The OTP should start and build the graph from given data. --inMemory declares that OTP will not store data into the disk. If you wish it to do so, you need to check the documentation of advanced usage.
OTP is quite memory intensive application, so you propably need to adjust the JAVA_MX -parameter according to your resources (size of the graph and the server)
After few moments (this can take over 20 minutes, depending on your graph size and resources. Be patient), the OTP should print "Grizzly server running.", after that the OTP server should be running in http://localhost:8080
